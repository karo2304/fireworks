const fireworksWrapEl = document.querySelector('.fireworks-wrapper');

Math.randomNumb = (min, max) => Math.round(Math.random() * (max - min) + min);

const createFirework = () => {
    const firework = {
        position: Math.randomNumb(0, window.innerWidth - 25),
        destination: Math.randomNumb(window.innerHeight / 2, window.innerHeight - 25),
        el: null,
        animationFrameId: null,
        bottom: 0,
        velocity: 15,
        thread: null,
        transitionTime: Math.randomNumb(2, 5),
        height: 50,
        whistleAudio: new Audio('./sounds/whistle.wav'),
        create: function() {
            this.whistleAudio.play();
            this.el = document.createElement('div');
            this.el.classList.add('firework');
            this.el.style.left = `${ this.position }px`;
            this.el.style.height = `${ this.height }px`;
            this.thread = (this.destination * 70) / 100;

            for (let i = 0; i < 40; i++) {
                const span = document.createElement("span");
                this.el.appendChild(span);
            }
        },
        transform: function() {
            this.bottom += this.velocity;

            if (this.bottom <= this.thread) {
                setTimeout(() => {
                    if (this.velocity > 0) {
                        this.velocity -= 0.15;

                        const height = parseInt(this.el.style.height);
                        this.el.style.height = `${ height - 1.3 }px`;
                    }
                }, 75);
            }

            this.el.style.bottom = `${ this.bottom }px`;
            this.animationFrameId = requestAnimationFrame(this.transform.bind(this));

            if (this.bottom >= this.thread) {
                window.cancelAnimationFrame(this.animationFrameId);

                const fireworkChildren = this.el.querySelectorAll('span');
                const fireworkColor = `rgb(${ Math.randomNumb(0, 255) }, ${ Math.randomNumb(0, 255) }, ${ Math.randomNumb(0, 255) })`;

                fireworkChildren.forEach((el, i) => {
                    el.style.transform = `translate(${ Math.randomNumb(-150, 150) }px, ${ Math.randomNumb(-150, 150) }px)`;
                    el.style.backgroundColor = fireworkColor;
                    el.style.boxShadow = `0px 0px 20px ${fireworkColor}`;
                    el.classList.add(`animation-${ Math.randomNumb(1, 3) }`);
                });

                this.el.style.backgroundColor = fireworkColor;
                this.el.style.boxShadow = `0px 0px 20px ${fireworkColor}`;
                this.el.style.opacity = '0';
                this.el.style.transition = `opacity ${ this.transitionTime }s ease`;

                setTimeout(() => this.el.remove(), 1000 * this.transitionTime);
            }
        }
    };

    firework.create();
    firework.transform();

    fireworksWrapEl.appendChild(firework.el);
};

setInterval(createFirework, 500);